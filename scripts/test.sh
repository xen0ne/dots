#!/usr/bin/env bash
clear
blockl=$(tput setab 7 && tput setaf 0 && echo -e "\u2591\u2592\u2593\u2588")
blockr=$(tput setab 1 && tput setaf 0 && echo -e "\u2591\u2592\u2593\u2588")

w=$(tput cols)
h=$(tput lines)
currl=$(($(tput lines) / 2))

draw() {
	str="${1}"
	len=${#str}
	tput cup $currl $(((w / 2) - (len / 2)))
	echo $str
	currl=$((currl + 1))
}

frame() {
	toprint=""
	for i in {1..7}; do
		toprint="$toprint$(tput setab $((i-1)) \
			&& tput setaf $i \
			&& echo -e "\u2591\u2592\u2593\u2588")"
	done
	tput cup $(((h / 2) - 4)) $(((w /2 ) - 16))
	echo $toprint$blockl
	toprint=""
	for i in {7..1}; do
		toprint="$toprint$(tput setab $((i+1)) \
			&& tput setaf $i \
			&& echo -e "\u2591\u2592\u2593\u2588")"
	done
	tput cup $(((h / 2) + 4)) $(((w /2 ) - 16))
	echo $toprint$blockr
	tput sgr0
	# draw sides
	for i in {1..7}; do
		tput cup $(((h / 2) - 4 + i)) $(((w /2 ) - 16))
		tput setaf $i
		echo -e "\u2591"
	done
	for i in {1..7}; do
		tput cup $(((h / 2) - 4 + (8 - i))) $(((w /2 ) + 14))
		tput setaf $i
		echo -e "\u2591"
	done
}

#frame
#draw xenone
#draw urxvt
#tput sgr0
#tput cup $(((h / 2) + 9)) 0

colorbar() {
	toprint=""
	for i in {1..7}; do
		toprint="$toprint$(tput setab $((i-1)) \
			&& tput setaf $i \
			&& echo -e "\u2591\u2592\u2593")"
			#&& echo -e "\u2591\u2592\u2593\u2588")"
	done
	echo $toprint$blockl
	tput sgr0
}

colorbar
echo -e "\u2591\t\t       \u2591"
echo -e	"\u2591\txenone\t       \u2591"
echo -e "\u2591\tmanjaro\t       \u2591"
echo -e "\u2591\tbspwm\t       \u2591"
echo -e "\u2591\turxvt\t       \u2591"
echo -e "\u2591\tlemonbar       \u2591"
echo -e "\u2591\t\t       \u2591"
colorbar
