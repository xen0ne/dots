c1="$(tput setaf 7)"
c2="$(tput setaf 3)"
c3="$(tput setaf 5)"
reset="$(tput sgr0)"

export PS1="\[$c3\]\u@\h \[$c2\]\w\n\[$c3\]$ \[$reset\]"

export PATH=$HOME/scripts:${PATH}
export PATH=$HOME/.cargo/bin:${PATH}
export PATH="$PATH:$HOME/data/pkgbuild/scripts"
export TERM=rxvt-unicode-256color

# aliases
alias kk="killall"
alias sourceb="source $HOME/.bashrc"
alias bashrc="nvim $HOME/.bashrc"
alias vimrc="nvim $HOME/.config/nvim/init.vim"
alias vim="nvim"
alias scripts="cd $HOME/scripts"
alias q="exit"
alias racket="/Applications/Racket\ v7.1/bin/racket"
alias merge="xrdb merge $HOME/.Xresources"
alias xres="nvim $HOME/.Xresources"
alias urxvtrc="nvim $HOME/.xfiles/urxvt"
alias sxhkdrc="cd $HOME/.config/sxhkd/"
alias bspwmrc="nvim $HOME/.config/bspwm/bspwmrc"
alias bat="cat /sys/class/power_supply/BAT0/capacity"
alias dots="git --git-dir=$HOME/dots/ --work-tree=$HOME"
alias siji="xfd -fn -wuncon-siji-medium-r-normal--10-100-75-75-c-80-iso10646-1"


export EDITOR=nvim
unset GREP_OPTIONS
#shopt -s autocd
shopt -s cdspell
if [ -f /etc/bash_completion ]; then
 . /etc/bash_completion
fi

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
